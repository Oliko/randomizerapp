﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace RandomizerModels
{
    [ServiceContract]
    public interface IRandomizerService
    {
        [OperationContract]
        bool UserExists(string username);

        [OperationContract]
        User GetUserByLoginAndPassword(string username, string password);

        [OperationContract]
        List<Randomizer> GetRandomizerByUserId(Guid userId);

        [OperationContract]
        void AddUser(User user);

        [OperationContract]
        void AddRandomizer(Randomizer randomizer);
    }
}
