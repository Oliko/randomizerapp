﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Entity.ModelConfiguration;
using Tools;
using ISerializable = Tools.ISerializable;

namespace RandomizerModels
{
    // Створюємо класс користувача, з атрибутами: Id, ім’я, прізвище, ім’я користувача, пошта, пароль, дата останнього логіну
    [DataContract] // Атрибут, щоб клас можна було серіалізувати
    public class User : ISerializable
    {
        [DataMember] // Атрибут, щоб можна було серіалізувати
        public string FileName { get; set; } = StaticResources.FileName;
                
        [DataMember] 
        public Guid Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Surname { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public DateTime DateOfLastLogIn { get; set; }
        [DataMember]
        public List<Randomizer> Queries { get; set; }

        // Створюємо конструктор
        public User()
        {
            DateOfLastLogIn = DateTime.Now;
            Queries = new List<Randomizer>();
        }

        // Створюємо ще один конструктор
        public User(string name, string surname, string username, string email, string password)
        {
            Id = Guid.NewGuid();
            Name = name;
            Surname = surname;
            Username = username;
            Email = email;
            Password = password;
            DateOfLastLogIn = DateTime.Now;
            Queries = new List<Randomizer>();
        }

        public class UserEntityConfiguration : EntityTypeConfiguration<User>
        {
            //Налаштування таблиці "Users" у БД
            public UserEntityConfiguration()
            {
                ToTable("Users");

                HasKey(s => s.Id);

                Property(p => p.Name)
                    .HasColumnName("Name")
                    .IsRequired();

                Property(p => p.Surname)
                    .HasColumnName("Surname")
                    .IsRequired();

                Property(p => p.Email)
                    .HasColumnName("Email")
                    .IsOptional();

                Property(p => p.Username)
                    .HasColumnName("Username")
                    .IsRequired();

                Property(p => p.Password)
                    .HasColumnName("Password")
                    .IsRequired();

                Property(p => p.DateOfLastLogIn)
                    .HasColumnName("DateOfLastLogIn")
                    .IsRequired();

                //У користувача може бути багато створених рандомайзерів - "запитів"
                HasMany(s => s.Queries)
                    .WithRequired(w => w.User)
                    .HasForeignKey(w => w.UserId)
                    .WillCascadeOnDelete(true);
            }
        }

    }
}
