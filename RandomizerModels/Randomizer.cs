﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace RandomizerModels
{
    // Створюємо класс Randomizer, з атрибутами: Id, початкове число, кінцеве число, кількість згенерованих елементів, дата генерації.
    [DataContract]   // Атрибут, щоб клас можна було серіалізувати

    public class Randomizer
    {
        [DataMember] // Атрибут, щоб можна було серіалізувати
        public Guid Id { get; set; }
        [DataMember]
        public int StartNumber { get; set; }
        [DataMember]
        public int EndNumber { get; set; }
        [DataMember]
        public int NumberOfGeneratedElements { get; set; }
        [DataMember]
        public DateTime DateOfGeneration { get; set; }
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public Guid UserId { get; set; }

        public Randomizer ()
        {
        }

        // Створюємо конструктор
        public Randomizer(int startNumber, int endNumber, Guid userId)
        {
            Id = Guid.NewGuid();
            StartNumber = startNumber;
            EndNumber = endNumber;
            NumberOfGeneratedElements = EndNumber - StartNumber + 1;
            DateOfGeneration = DateTime.Now;
            UserId = userId;
        }

        // Створюємо метод, який повертяє згенеровану послідовність, запускаємо потік
        public async Task<int[]> GenerateSequence()
        {

            return await Task.Run(() =>
            {
                Thread.Sleep(1000);

                int[] startedArray = Enumerable.Range(StartNumber, NumberOfGeneratedElements).ToArray();
                Random random = new Random();

                for (int i = NumberOfGeneratedElements - 1; i >= 1; i--)
                {
                    int j = random.Next(i + 1);
                    int temp = startedArray[j];
                    startedArray[j] = startedArray[i];
                    startedArray[i] = temp;
                }

                return startedArray;
            });

        }

        public class RandomizerEntityConfiguration : EntityTypeConfiguration<Randomizer>
        {
            //Налаштування таблиці "Users" у БД
            public RandomizerEntityConfiguration()
            {
                ToTable("Randomizer");

                HasKey(s => s.Id);

                Property(p => p.StartNumber)
                    .HasColumnName("StartNumber")
                    .IsRequired();

                Property(p => p.EndNumber)
                    .HasColumnName("EndNumber")
                    .IsRequired();

                Property(p => p.NumberOfGeneratedElements)
                    .HasColumnName("NumberOfGeneratedElements")
                    .IsOptional();

                Property(p => p.DateOfGeneration)
                    .HasColumnName("DateOfGeneration")
                    .IsRequired();

                //Кожен "рандомайзер" належить одному користувачу
                HasRequired(p => p.User)
                    .WithMany(u => u.Queries)
                    .HasForeignKey(p => p.UserId);
            }
        }

    }
}
