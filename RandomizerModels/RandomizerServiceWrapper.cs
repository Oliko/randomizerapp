﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace RandomizerModels
{
    public class RandomizerServiceWrapper
    {
        public static bool UserExists(string username)
        {
            using (var myChannelFactory = new ChannelFactory<IRandomizerService>("Server"))
            {
                var client = myChannelFactory.CreateChannel();
                return client.UserExists(username);
            }
        }

        public static User GetUserByLoginAndPassword(string username, string password)
        {
            using (var myChannelFactory = new ChannelFactory<IRandomizerService>("Server"))
            {
                var client = myChannelFactory.CreateChannel();
                return client.GetUserByLoginAndPassword(username, password);
            }
        }

        public static List<Randomizer> GetRandomizerByUserId(Guid userId)
        {
            using (var myChannelFactory = new ChannelFactory<IRandomizerService>("Server"))
            {
                var client = myChannelFactory.CreateChannel();
                return client.GetRandomizerByUserId(userId);
            }
        }

        public static void AddUser(User user)
        {
            using (var myChannelFactory = new ChannelFactory<IRandomizerService>("Server"))
            {
                var client = myChannelFactory.CreateChannel();
                client.AddUser(user);
            }
        }

        public static void AddRandomizer(Randomizer randomizer)
        {
            using (var myChannelFactory = new ChannelFactory<IRandomizerService>("Server"))
            {
                var client = myChannelFactory.CreateChannel();
                client.AddRandomizer(randomizer);
            }
        }
    }
}