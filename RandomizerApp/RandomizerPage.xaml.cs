﻿using System.Windows;
using System.Windows.Controls;
using RandomizerApp.Helpers;
using Tools;
using static System.String;

namespace RandomizerApp
{
    // Створюємо сторінку для генерації і кнопки виходу, історії та генерації
    public partial class RandomizerPage : Page
    {
        public RandomizerPage()
        {
            InitializeComponent();

            if (IsNullOrEmpty(StationManager.CurrentUser.Username))
            {
                MessageBox.Show("You should log in!");
                Logger.Log("User trying open Randomizer page without being logged in");
                return;
            }

            CurrentUserUsername.Content = StationManager.CurrentUser.Username;
            CurrentUserName.Content = StationManager.CurrentUser.Name + " " + StationManager.CurrentUser.Surname;
            CurrentUserEmail.Content = StationManager.CurrentUser.Email;

            RandomizerUserControl randomizerUserControl = new RandomizerUserControl();
            ContentControl.Content = randomizerUserControl;
        }

        private void GenerateNew_Click(object sender, RoutedEventArgs e)
        {
            RandomizerUserControl randomizerUserControl = new RandomizerUserControl();
            ContentControl.Content = randomizerUserControl;
            Logger.Log("Open Generate New tab");
        }

        private void History_Click(object sender, RoutedEventArgs e)
        {
            HistoryUserControl historyUserControl = new HistoryUserControl();
            ContentControl.Content = historyUserControl;
            Logger.Log("Open History tab");
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            StationManager.CurrentUser = null;
            SerializeManager.DeleteFile(StaticResources.FileName);
            Logger.Log("User log out");

            Logger.Log("Open Login page");
            Helper.OpenPage(this, "LoginPage", "Authorization");
        }
    }
}
