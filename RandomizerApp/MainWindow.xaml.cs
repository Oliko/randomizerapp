﻿using System;
using System.Windows;
using RandomizerApp.Helpers;
using RandomizerModels;
using Tools;

namespace RandomizerApp
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            User loggedUser = SerializeManager.Deserialize<User>(StaticResources.FileName);

            //Якщо немає залогіненого користувача
            if (!String.IsNullOrEmpty(loggedUser.Username))
            {
                StationManager.CurrentUser = loggedUser;
                Logger.Log("User with username " + StationManager.CurrentUser.Username +
                           " has been logged, so open Randomizer page");
                MainFrame.Source = new Uri("RandomizerPage.xaml", UriKind.Relative);
            }
            else
            {
                Logger.Log("There are no logged user, so open Login page");
                MainFrame.Source = new Uri("Authorization/LoginPage.xaml", UriKind.Relative);
            }
        }
    }
}
