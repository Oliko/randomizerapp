﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FontAwesome.WPF;
using RandomizerApp.Helpers;
using static System.String;
using RandomizerModels;
using Tools;

namespace RandomizerApp
{
    public partial class RandomizerUserControl : UserControl
    {
        private ImageAwesome _loader;

        public RandomizerUserControl()
        {
            InitializeComponent();
        }

        private void OnRequestLoader(bool isShow)
        {
            if (isShow && _loader == null)
            {
                _loader = new ImageAwesome();
                RandomizerGrid.Children.Add(_loader);
                _loader.Icon = FontAwesomeIcon.Spinner;
                _loader.Spin = true;
                Grid.SetRow(_loader, 1);
                Grid.SetColumnSpan(_loader, 2);
                IsEnabled = false;
            }
            else if (_loader != null)
            {
                RandomizerGrid.Children.Remove(_loader);
                _loader = null;
                IsEnabled = true;
            }
        }

        // Створуємо кнопку генерації
        private async void Generate_Click(object sender, RoutedEventArgs e)
        {
            // Якщо початкове або кінцеве число не введено, то  вибиває повідомлення, що треба зповнити обидва поля
            if (IsNullOrEmpty(StartNumber.Text) || IsNullOrEmpty(EndNumber.Text))
            {
                MessageBox.Show("Please, fill both fields!");
                Logger.Log("User does not fill both field with numbers");
                return;
            }

            int startNum, endNum;
            Guid userId = StationManager.CurrentUser.Id;

            // Якщо початкове або кінцеве число(поле) після перетворення не є числом int, то виводить повідомлення, що треба ввести числа в обидва поля
            if (!int.TryParse(StartNumber.Text, out startNum) || !int.TryParse(EndNumber.Text, out endNum))
            {
                MessageBox.Show("Please, write numbers in both fields!");
                Logger.Log("User fill field with no-numbers characters");
                return;
            }
            
            // Якщо початкове число більше кінцевого, то виводить повідомлення, що має бути навпаки
            if (startNum >= endNum)
            {
                MessageBox.Show("End number should be greater thar start number!");
                Logger.Log("User fill start field with greater number than in end field");
                return;
            }

            // Якщо число не знаходиться на проміжку від -100000 до 100000
            if (startNum < -100000 || 100000 < endNum)
            {
                MessageBox.Show("Your numbers` range should be in -100000 to 100000!");
                Logger.Log("User fill field with too big number");
                return;
            }

            OnRequestLoader(true);

            Randomizer userRandomizer = new Randomizer(startNum, endNum, userId);
            Logger.Log("New Randomizer created");
            //Додавання рандомайзера до БД
            RandomizerServiceWrapper.AddRandomizer(userRandomizer);
            Logger.Log("New Randomizer has been added to database");

            int[] seq = await userRandomizer.GenerateSequence();
            string[] result = seq.Select(x => x.ToString()).ToArray();

            ResultListBox.ItemsSource = result;

            OnRequestLoader(false);
        }
    }
}
