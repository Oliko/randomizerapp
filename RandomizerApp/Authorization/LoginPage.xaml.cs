﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FontAwesome.WPF;
using RandomizerApp.Helpers;
using RandomizerModels;
using Tools;

namespace RandomizerApp.Authorization
{
    // Створюємо сторінку входу, з кнопками входу та реєстрації
    public partial class LoginPage : Page
    {
        private ImageAwesome _loader;

        public LoginPage()
        {
            InitializeComponent();
        }

        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            Logger.Log("Open SignUp page");
            Helper.OpenPage(this, "SignUpPage", "Authorization");        
        }

        // Створюємо лоадер-спіннер
        private void OnRequestLoader(bool isShow)
        {
            if (isShow && _loader == null)
            {
                _loader = new ImageAwesome();
                LoginGrid.Children.Add(_loader);
                _loader.Icon = FontAwesomeIcon.Spinner;
                _loader.Spin = true;
                Grid.SetRow(_loader, 2);
                Grid.SetColumnSpan(_loader, 2);
                IsEnabled = false;
            }
            else if (_loader != null)
            {
                LoginGrid.Children.Remove(_loader);
                _loader = null;
                IsEnabled = true;
            }
        }

        // Вхід відбувається та відкривається сторінка генерації, якщо і пароль і юзернейм правильно введені, та якщо юзер з введеними даними існує
        private async void LogIn_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Username.Text) || String.IsNullOrEmpty(Password.Password))
            {
                MessageBox.Show("Username or Password are not set");
                Logger.Log("User does not enter username or password");
                return;
            }

            var username = Username.Text;
            var password = Password.Password;

            // Виводиться лоадер та запускається потік
            OnRequestLoader(true);
            await Task.Run(() =>
            {
                var currentUser = RandomizerServiceWrapper.GetUserByLoginAndPassword(username, password);

                if (currentUser == null)
                {
                    MessageBox.Show("Wrong Username or Password");
                    Logger.Log("User enter wrong username or password");
                    return false;
                }

                StationManager.CurrentUser = currentUser;
                SerializeManager.Serialize(currentUser);

                return true;
            });

            // Лоадер вимикається
            OnRequestLoader(false);

            Logger.Log("Open Randomizer page");
            Helper.OpenPage(this, "RandomizerPage");
        }

    }
}
