﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FontAwesome.WPF;
using RandomizerApp.Helpers;
using RandomizerModels;
using Tools;

namespace RandomizerApp.Authorization
{
    // Сторінка реєстрації
    public partial class SignUpPage : Page
    {
        private ImageAwesome _loader;

        public SignUpPage()
        {
            InitializeComponent();
        }

        // Створюємо лоадер-спіннер
        private void OnRequestLoader(bool isShow)
        {
            if (isShow && _loader == null)
            {
                _loader = new ImageAwesome();
                SignUpGrid.Children.Add(_loader);
                _loader.Icon = FontAwesomeIcon.Spinner;
                _loader.Spin = true;
                Grid.SetRow(_loader, 2);
                Grid.SetColumnSpan(_loader, 2);
                IsEnabled = false;
            }
            else if (_loader != null)
            {
                SignUpGrid.Children.Remove(_loader);
                _loader = null;
                IsEnabled = true;
            }
        }

        // Перевірка на відповідність символам пошти
        private bool ValidateEmail(string email)
        {
            string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            return Regex.IsMatch(email, pattern);
        }

        // Реєстрація відбувається, новий юзер ддодається до бази та відкривається сторінка генераціїї, якщо всі поля заповнені, пошта відопідає певним умовам та такого юзера в базі не існує
        private async void SignUp_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Name.Text) ||
                String.IsNullOrEmpty(Surname.Text) ||
                String.IsNullOrEmpty(Username.Text) ||
                String.IsNullOrEmpty(Email.Text) ||
                String.IsNullOrEmpty(Password.Password))
            {
                MessageBox.Show("Please, fill all fields!");
                Logger.Log("User does not fill all fields");
                return;
            }

            if (!ValidateEmail(Email.Text))
            {
                MessageBox.Show("Invalid email!");
                Logger.Log("User enter invalid email");
                return;
            }

            var name = Name.Text;
            var surname = Surname.Text;
            var username = Username.Text;
            var email = Email.Text;
            var password = Password.Password;

            // Виводиться лоадер та запускається потік
            OnRequestLoader(true);

            await Task.Run(() =>
            {
                //Перевірка чи існує у БД користувач з таким же юзернеймом
                //Якщо так, то треба вигадати інший юзернейм
                if (RandomizerServiceWrapper.UserExists(username))
                {
                    MessageBox.Show("User with this username already exists");
                    Logger.Log("User trying to log in with existing username");
                    return false;
                }

                var currentUser = new User(
                    name,
                    surname,
                    username,
                    email,
                    password);

                //Додавання користувача у БД
                RandomizerServiceWrapper.AddUser(currentUser);

                Logger.Log("User has been added to DB");
                StationManager.CurrentUser = currentUser;
                SerializeManager.Serialize(currentUser);

                return true;
            });

            // Лоадер вимикається
            OnRequestLoader(false);          

            Logger.Log("Open Randomizer page");
            Helper.OpenPage(this, "RandomizerPage", null);
        }

        // Повертає на головну сторінку входу
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Logger.Log("Open Login page");
            Helper.OpenPage(this, "LoginPage", "Authorization");          
        }

    }
}
