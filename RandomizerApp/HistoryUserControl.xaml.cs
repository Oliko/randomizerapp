﻿using System;
using System.Linq;
using System.Windows.Controls;
using RandomizerApp.Helpers;
using RandomizerModels;
using Tools;

namespace RandomizerApp
{
    public partial class HistoryUserControl : UserControl
    {
        public HistoryUserControl()
        {
            InitializeComponent();

            try
            {
                Guid userId = StationManager.CurrentUser.Id;
                if (StationManager.CurrentUser != null && RandomizerServiceWrapper.GetRandomizerByUserId(userId).Any())
                {
                    //Виводимо усі рандомайзери користувача
                    foreach (var query in RandomizerServiceWrapper.GetRandomizerByUserId(userId))
                    {
                        QueriesListView.Items.Add(query);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log("Problems with displaying history", e);
                throw;
            }
        }
    }
}