namespace DBAdapter.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Randomizer",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartNumber = c.Int(nullable: false),
                        EndNumber = c.Int(nullable: false),
                        NumberOfGeneratedElements = c.Int(),
                        DateOfGeneration = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FileName = c.String(),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Username = c.String(nullable: false),
                        Email = c.String(),
                        Password = c.String(nullable: false),
                        DateOfLastLogIn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Randomizer", "UserId", "dbo.Users");
            DropIndex("dbo.Randomizer", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.Randomizer");
        }
    }
}
