﻿using System;
using System.Collections.Generic;
using System.Linq;
using RandomizerModels;

namespace DBAdapter
{
    //Клас для зв'язку з БД
    public class EntityWrapper
    {
        //Перевірка чи існує користувач з таким юзернеймом
        public static bool UserExists(string username)
        {
            using (var context = new RandomizerContext())
            {
                return context.Users.Any(u => u.Username == username);
            }
        }

        //Знайти користувача з певним юзернеймом та паролем
        public static User GetUserByLoginAndPassword(string username, string password)
        {
            using (var context = new RandomizerContext())
            {
                return context.Users.FirstOrDefault(u => u.Username == username && u.Password == password);
            }
        }

        //Список усіх рандомайзерів користувача
        public static List<Randomizer> GetRandomizerByUserId(Guid userId)
        {
            using (var context = new RandomizerContext())
            {
                return context.Randomizers.AsNoTracking().Where(r => r.UserId == userId).ToList();
            }
        }

        //Додати користувача до БД
        public static void AddUser(User user)
        {
            using (var context = new RandomizerContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        //Додати рандомайзер до БД
        public static void AddRandomizer(Randomizer randomizer)
        {
            using (var context = new RandomizerContext())
            {
                context.Randomizers.Add(randomizer);
                context.SaveChanges();
            }
        }
    }
}
