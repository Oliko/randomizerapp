﻿using System.Data.Entity;
using DBAdapter.Migrations;
using RandomizerModels;

namespace DBAdapter
{

    public class RandomizerContext : DbContext
    {
        public RandomizerContext()
            : base("UI")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<RandomizerContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<RandomizerContext, Configuration>("UI"));
        }

        //Таблиці в БД
        public DbSet<User> Users { get; set; }
        public DbSet<Randomizer> Randomizers { get; set; }

        //Налаштування для полів таблиць
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new User.UserEntityConfiguration());
            modelBuilder.Configurations.Add(new Randomizer.RandomizerEntityConfiguration());
        }
    }
}