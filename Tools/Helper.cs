﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Tools
{
    public class Helper
    {
        public static void OpenPage(Page thisPage, string pageToOpen, string folder=null)
        {
            Frame mainFrame = null;
            DependencyObject currParent = VisualTreeHelper.GetParent(thisPage);
            while (currParent != null && mainFrame == null)
            {
                mainFrame = currParent as Frame;
                currParent = VisualTreeHelper.GetParent(currParent);
            }

            if (mainFrame != null)
            {
                if (folder==null)
                {
                    mainFrame.Source = new Uri(pageToOpen + ".xaml", UriKind.Relative);
                }
                else
                {
                    mainFrame.Source = new Uri(folder + "/" + pageToOpen + ".xaml", UriKind.Relative);
                }
            }
        }
    }
}
