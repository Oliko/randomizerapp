﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Tools
{
    public interface ISerializable
    {
        string FileName { get; }
    }

    public static class SerializeManager
    {
        // Сторюємо директорію, куда хочемо покласти json, перевіривши чи існує вже така
        private static string CreateAndGetPath(string filename)
        {
            if (!Directory.Exists(StaticResources.DirPath))
                Directory.CreateDirectory(StaticResources.DirPath);

            return Path.Combine(StaticResources.DirPath, filename);
        }

        // Видалення файлу за вказаним шляхом
        public static void DeleteFile(string filename)
        {
            string fileNameToDelete = CreateAndGetPath(filename);
            if (File.Exists(fileNameToDelete))
                File.Delete(fileNameToDelete);
        }

        // Перетворення об'єкта в потік байтів
        public static void Serialize<TObject>(TObject obj) where TObject : ISerializable
        {
            // Обробка винятків

            // Метод WriteObject приймає створений файловий поток та об'єкт, та робить запис об'єкта в json-файл
            try
            {
                DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(TObject));
                var fileNameToSerialize = CreateAndGetPath(obj.FileName);

                using (FileStream fs = new FileStream(fileNameToSerialize, FileMode.OpenOrCreate))
                {
                    formatter.WriteObject(fs, obj);
                }
            }
            catch (Exception e)
            {
                Logger.Log("Problems with Serialization", e);
                throw;
            }
        }

        // Отримання об'ектів з потоку байтів
        public static TObject Deserialize<TObject>(string filename) where TObject : ISerializable, new()
        {
            try
            {
                // Обробка винятків

                // Метод ReadObject приймає створений файловий поток, який представляє файл в форматі json

                DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(TObject));
                var fileNameToDeserialize = CreateAndGetPath(filename);

                //if there are no logged user
                if (!File.Exists(fileNameToDeserialize))
                    return new TObject();

                using (FileStream fs = new FileStream(fileNameToDeserialize, FileMode.OpenOrCreate))
                {
                    return (TObject)formatter.ReadObject(fs);
                }
            }
            catch (Exception e)
            {
                Logger.Log("Problems with Deserialization", e);
                throw;
            }
        }
    }
}