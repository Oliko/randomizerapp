﻿using System;
using System.IO;
using System.Text;

namespace Tools
{
    public static class Logger
    {
        private static readonly string Filepath = Path.Combine(StaticResources.LogDirPath,
            "Session_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt");


        // Перевірка чи є папка та файл логів
        public static void CheckingCreateFile()
        {
            if (!Directory.Exists(StaticResources.LogDirPath))
            {
                Directory.CreateDirectory(StaticResources.LogDirPath);
            }
            if (!File.Exists(Filepath))
            {
                File.Create(Filepath).Close();
            }
        }

        // Створення логу
        public static void Log(string message)
        {
            CheckingCreateFile();

            using (StreamWriter sw = new StreamWriter(Filepath, true, Encoding.Default))
            {
                sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " - " + message);
            }
        }

        // Запис помилки в лозі
        public static void Log(string message, Exception ex)
        {
            CheckingCreateFile();

            Log(message);
            var realException = ex;
            while (realException != null)
            {
                Log(realException.Message);
                Log(realException.StackTrace);
                realException = realException.InnerException;
            }
        }
    }
}