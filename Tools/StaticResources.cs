﻿using System;
using System.IO;

namespace Tools
{
    public static class StaticResources
    {
        public static readonly string FileName = "user.json";
        private static readonly string AppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        internal static readonly string DirPath = Path.Combine(AppData, "RandomizerApp");
        internal static readonly string LogDirPath = Path.Combine(DirPath, "Logs");
    }
}
