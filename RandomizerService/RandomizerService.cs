﻿using System;
using System.Collections.Generic;
using DBAdapter;
using RandomizerModels;

namespace RandomizerService
{
    public class RandomizerService : IRandomizerService
    {
        public bool UserExists(string username)
        {
            return EntityWrapper.UserExists(username);
        }

        public User GetUserByLoginAndPassword(string username, string password)
        {
            return EntityWrapper.GetUserByLoginAndPassword(username, password);
        }

        public List<Randomizer> GetRandomizerByUserId(Guid userId)
        {
            return EntityWrapper.GetRandomizerByUserId(userId);
        }

        public void AddUser(User user)
        {
            EntityWrapper.AddUser(user);
        }

        public void AddRandomizer(Randomizer randomizer)
        {
            EntityWrapper.AddRandomizer(randomizer);
        }
    }
}
